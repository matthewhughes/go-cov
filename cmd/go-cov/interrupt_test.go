//go:build !windows

package main

import (
	"bytes"
	"context"
	"io"
	"syscall"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"
	"gitlab.com/matthewhughes/signalctx"
)

// not sure how reliable this test is on non Linux systems...
func TestApp_ExitsOnSigint(t *testing.T) {
	signal := syscall.SIGINT
	expectedCode := 130
	expectedErr := "interrupted (^C)"
	interruptedCtx, cancel := signalctx.NotifyContext(context.Background(), signal)
	defer cancel()

	require.NoError(t, syscall.Kill(syscall.Getpid(), signal))
	assert.Eventually(
		t,
		func() bool { return interruptedCtx.Err() != nil },
		time.Second,
		10*time.Millisecond,
	)

	var buf bytes.Buffer
	// suppress printing help
	cli.HelpPrinter = func(_ io.Writer, _ string, _ any) {}
	exit, err := runApp(interruptedCtx, nil, &buf, []string{"execName"})

	assert.Equal(t, expectedCode, exit)
	assert.EqualError(t, err, expectedErr)
}
