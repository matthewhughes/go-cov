package testdata

// to generate bad.coverage uncomment the line below
// and run `go test -coverprofile syntax-error.cover ./..`
SYNTAX_ERROR

func foo() int {
	return 1
}
