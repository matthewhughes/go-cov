package testdata

func notCalled() int { //some-other-tool:blah //go-cov:skip
	if true {
		return 2
	}
	return 1
}

func lotsOfBranching() int {
	if false { //go-cov:skip
		return 0
	}

	ret := 0
	if false { //go-cov:skip
		ret++
	} else {
		ret++
	}

	if true {
		ret--
	} else { //go-cov:skip
		ret++
	}

	for i := 0; false && i < 10; i++ { //go-cov:skip
		ret++
	}

	switch { //go-cov:skip
	case false:
		ret++
	}

	return ret
}
