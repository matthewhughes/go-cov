package testdata

//go-cov:skip

func skipped1() bool {
	if true {
		return true
	}
	return false
}

func skipped2() int {
	count := 0
	for i := 0; i < 10; i++ {
		count++
	}
	return count
}
