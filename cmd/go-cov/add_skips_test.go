package main

import (
	"bytes"
	"context"
	"fmt"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
	"golang.org/x/tools/cover"
)

const _testdataDir = "testdata"

var _args = []string{"execName", "add-skips"}

func TestAddSkips_ErrorsOnWrongNumberOfArgs(t *testing.T) {
	for _, tc := range []struct {
		cmdArgs     []string
		expectedErr string
	}{
		{
			[]string{},
			"takes exactly one argument, but 0 provided",
		},
		{
			[]string{"first-file", "second-file"},
			"takes exactly one argument, but 2 provided",
		},
	} {
		t.Run(fmt.Sprintf("%d _args", len(tc.cmdArgs)), func(t *testing.T) {
			args := append(_args, tc.cmdArgs...)
			var buf bytes.Buffer
			exit, err := runApp(context.Background(), nil, &buf, args)

			require.Equal(t, 1, exit)
			require.EqualError(t, err, tc.expectedErr)
		})
	}
}

func TestAddSkips_ErrorsOnPackageListFailure(t *testing.T) {
	badPackageFile := filepath.Join(_testdataDir, "bad-package.cover")
	var buf bytes.Buffer
	args := append(_args, badPackageFile)

	exit, err := runApp(context.Background(), nil, &buf, args)

	require.Equal(t, 1, exit)
	require.ErrorContains(t, err, "listing paths for packages: ")
}

func TestAddSkips_ErrorsOnBadCoverFile(t *testing.T) {
	var buf bytes.Buffer
	badFileName := "some-invalid-file"
	args := append(_args, badFileName)

	exit, err := runApp(context.Background(), nil, &buf, args)

	require.Equal(t, 1, exit)
	require.ErrorContains(t, err, "parsing profiles from "+badFileName+":")
}

func TestAddSkips_ErrorsOnUnParseableSourceFile(t *testing.T) {
	badcoverFile := filepath.Join(_testdataDir, "syntax-error", "syntax-error.cover")
	badFile, err := filepath.Abs(filepath.Join(_testdataDir, "syntax-error", "syntax_error.go"))
	require.NoError(t, err)

	var buf bytes.Buffer
	args := append(_args, badcoverFile)

	exit, err := runApp(context.Background(), nil, &buf, args)

	require.Equal(t, 1, exit)
	require.ErrorContains(t, err, "parsing "+badFile+":")
}

func TestAddSkips_SucceedsOnEmptyFile(t *testing.T) {
	emptyfile := filepath.Join(_testdataDir, "empty.cover")
	var buf bytes.Buffer
	args := append(_args, emptyfile)

	exit, err := runApp(context.Background(), nil, &buf, args)

	require.NoError(t, err)
	require.Equal(t, 0, exit)
	require.Equal(t, "", buf.String())
}

func TestAddSkips_WithGeneratedProfiles(t *testing.T) {
	for _, coverFile := range []string{"examples.cover", "examples-filepaths.cover"} {
		t.Run(coverFile, func(t *testing.T) {
			coverPath := filepath.Join(_testdataDir, coverFile)
			coverPathExt := filepath.Ext(coverPath)
			expectedCoverPath := coverPath[:len(coverPath)-len(coverPathExt)] + ".expected" + coverPathExt
			var buf bytes.Buffer
			args := append(_args, coverPath)

			exit, err := runApp(context.Background(), nil, &buf, args)
			require.NoError(t, err)
			require.Equal(t, 0, exit)

			gotProfiles, err := cover.ParseProfilesFromReader(&buf)
			require.NoError(t, err)
			expectedProfiles, err := cover.ParseProfiles(expectedCoverPath)
			require.NoError(t, err)

			require.Equal(t, expectedProfiles, gotProfiles)
		})
	}
}
