package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

var _reportArgs = []string{"execName", "report"}

func TestReport_ErrorsOnTooManyArgs(t *testing.T) {
	args := append(_reportArgs, "first-file", "second-file")
	exit, err := runApp(context.Background(), nil, io.Discard, args)

	require.EqualError(t, err, "takes at most one argument, but 2 provided")
	require.Equal(t, 1, exit)
}

func TestReport_ErrorsOnBadCoverFile(t *testing.T) {
	badFileName := "some-invalid-file"
	args := append(_reportArgs, badFileName)

	exit, err := runApp(context.Background(), nil, io.Discard, args)

	require.ErrorContains(t, err, "parsing profiles from "+badFileName+":")
	require.Equal(t, 1, exit)
}

func TestReport_FailsOnUnparseableProfiles(t *testing.T) {
	badCoverContent := "this is not a cover file\n"

	t.Run("from file", func(t *testing.T) {
		covFile := filepath.Join(t.TempDir(), "coverage.out")
		expectedErrPrefix := "parsing profiles from " + covFile + ": "
		args := append(_reportArgs, covFile)

		require.NoError(t, os.WriteFile(covFile, []byte(badCoverContent), 0o600))

		exit, err := runApp(context.Background(), nil, io.Discard, args)

		require.ErrorContains(t, err, expectedErrPrefix)
		require.Equal(t, 1, exit)
	})

	t.Run("from input", func(t *testing.T) {
		in := bytes.NewBufferString(badCoverContent)
		expectedErrPrefix := "parsing profiles from input: "

		exit, err := runApp(context.Background(), in, io.Discard, _reportArgs)

		require.ErrorContains(t, err, expectedErrPrefix)
		require.Equal(t, 1, exit)
	})
}

func TestReport_FullCoverage(t *testing.T) {
	covContent := `mode: set
example.com/some/pkg/file.go:3.22,4.10 1 1
example.com/some/pkg/file.go:4.10,6.3 1 1
example.com/some/pkg/file.go:7.2,7.10 1 1
example.com/some/pkg/file.go:10.28,11.11 1 1
example.com/some/pkg/file.go:11.11,13.3 1 1
example.com/some/pkg/other_file.go:3.23,5.2 1 1
`
	expectedOut := "coverage: 100.00%\n"
	t.Run("from file", func(t *testing.T) {
		covFile := filepath.Join(t.TempDir(), "coverage.out")
		args := append(_reportArgs, covFile)
		var out bytes.Buffer

		require.NoError(t, os.WriteFile(covFile, []byte(covContent), 0o600))

		exit, err := runApp(context.Background(), nil, &out, args)

		require.NoError(t, err)
		require.Equal(t, 0, exit)
		require.Equal(t, expectedOut, out.String())
	})

	t.Run("from input", func(t *testing.T) {
		in := bytes.NewBufferString(covContent)
		var out bytes.Buffer

		exit, err := runApp(context.Background(), in, &out, _reportArgs)

		require.NoError(t, err)
		require.Equal(t, 0, exit)
		require.Equal(t, expectedOut, out.String())
	})
}

func TestReport_MissingCoverage(t *testing.T) {
	covContent := `mode: set
example.com/some/pkg/file.go:3.22,4.10 1 1
example.com/some/pkg/file.go:4.10,6.3 1 1
example.com/some/pkg/file.go:7.2,7.10 1 0
example.com/some/pkg/file.go:10.28,11.11 1 1
example.com/some/pkg/file.go:11.11,13.3 1 0
example.com/some/pkg/other_file.go:3.23,5.2 1 0
`
	expectedOut := strings.Join(
		[]string{
			"Uncovered block in example.com/some/pkg/file.go at (line, col) (7, 2)",
			"Uncovered block in example.com/some/pkg/file.go at (line, col) (11, 11)",
			"Uncovered block in example.com/some/pkg/other_file.go at (line, col) (3, 23)",
			"coverage: 50.00%\n",
		},
		"\n",
	)
	t.Run("from file", func(t *testing.T) {
		covFile := filepath.Join(t.TempDir(), "coverage.out")
		args := append(_reportArgs, covFile)
		var out bytes.Buffer

		require.NoError(t, os.WriteFile(covFile, []byte(covContent), 0o600))

		exit, err := runApp(context.Background(), nil, &out, args)

		require.NoError(t, err)
		require.Equal(t, 0, exit)
		require.Equal(t, expectedOut, out.String())
	})

	t.Run("from input", func(t *testing.T) {
		in := bytes.NewBufferString(covContent)
		var out bytes.Buffer

		exit, err := runApp(context.Background(), in, &out, _reportArgs)

		require.NoError(t, err)
		require.Equal(t, 0, exit)
		require.Equal(t, expectedOut, out.String())
	})
}

func TestReport_MissingCoverage_UnderLimit(t *testing.T) {
	covContent := `mode: set
example.com/some/pkg/file.go:3.22,4.10 1 1
example.com/some/pkg/file.go:4.10,6.3 1 1
example.com/some/pkg/file.go:7.2,7.10 1 0
example.com/some/pkg/file.go:10.28,11.11 1 1
example.com/some/pkg/file.go:11.11,13.3 1 0
example.com/some/pkg/other_file.go:3.23,5.2 1 0
`

	covPercent := "50.00%"
	expectedOut := strings.Join(
		[]string{
			"Uncovered block in example.com/some/pkg/file.go at (line, col) (7, 2)",
			"Uncovered block in example.com/some/pkg/file.go at (line, col) (11, 11)",
			"Uncovered block in example.com/some/pkg/other_file.go at (line, col) (3, 23)",
			fmt.Sprintf("coverage: %s\n", covPercent),
		},
		"\n",
	)

	for _, tc := range []struct {
		failUnder      string
		expectedFormat string
	}{
		{
			"65",
			"65.00%",
		},
		{
			"71.0",
			"71.00%",
		},
		{
			"85.123",
			"85.12%",
		},
	} {
		t.Run(tc.failUnder, func(t *testing.T) {
			expectedErr := fmt.Sprintf(
				"coverage (%s) below limit (%s)",
				covPercent,
				tc.expectedFormat,
			)
			args := append(_reportArgs, "--fail-under", tc.failUnder)

			t.Run("from file", func(t *testing.T) {
				covFile := filepath.Join(t.TempDir(), "coverage.out")
				args := append(args, covFile)
				var out bytes.Buffer

				require.NoError(t, os.WriteFile(covFile, []byte(covContent), 0o600))

				exit, err := runApp(context.Background(), nil, &out, args)

				require.EqualError(t, err, expectedErr)
				require.Equal(t, 1, exit)
				require.Equal(t, expectedOut, out.String())
			})

			t.Run("from input", func(t *testing.T) {
				in := bytes.NewBufferString(covContent)
				var out bytes.Buffer

				exit, err := runApp(context.Background(), in, &out, args)

				require.EqualError(t, err, expectedErr)
				require.Equal(t, 1, exit)
				require.Equal(t, expectedOut, out.String())
			})
		})
	}
}
