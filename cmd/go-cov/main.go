package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/matthewhughes/signalctx"

	"gitlab.com/matthewhughes/go-cov/internal"
)

func main() { //go-cov:skip
	exitCode, err := runApp(context.Background(), os.Stdin, os.Stdout, os.Args)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}

	os.Exit(exitCode)
}

const (
	_exitSuccess = 0
	_exitFailure = 1
	// https://tldp.org/LDP/abs/html/exitcodes.html
	_signalExitBase = 128
)

func runApp(ctx context.Context, in io.Reader, out io.Writer, args []string) (int, error) {
	ctx, cancel := signalctx.NotifyContext(ctx, os.Interrupt)
	defer cancel()

	cli := cli.App{
		Name:  "go-cov",
		Usage: "Calculate and report test coverage with explicit coverage skip",
		Commands: []*cli.Command{
			internal.AddSkipsCmd(out),
			internal.ReportCmd(in, out),
		},
	}

	if err := cli.RunContext(ctx, args); err != nil {
		return _exitFailure, err //nolint:wrapcheck
	}
	if err := signalctx.FromContext(ctx); err != nil {
		if err.Signal == os.Interrupt {
			return _signalExitBase + 2, errors.New("interrupted (^C)")
		}
	}
	return _exitSuccess, nil
}
