module gitlab.com/matthewhughes/go-cov

go 1.23.0

require (
	dmitri.shuralyov.com/go/generated v0.0.0-20230423232055-e1de01541153
	github.com/stretchr/testify v1.10.0
	github.com/urfave/cli/v2 v2.27.5
	gitlab.com/matthewhughes/mages v0.2.0
	gitlab.com/matthewhughes/signalctx v0.1.0
	golang.org/x/tools v0.30.0
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.6 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/magefile/mage v1.15.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20240521201337-686a1a2994c1 // indirect
	golang.org/x/mod v0.23.0 // indirect
	golang.org/x/sync v0.11.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
