package internal

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_findSkippedBlocksSuccess(t *testing.T) {
	testCases := []struct {
		desc           string
		fileContent    string
		expectedBlocks []skippedBlock
	}{
		{
			"empty file",
			"package somePackage\n",
			[]skippedBlock{},
		},
		{
			"skipped function",
			`package somePackage

			func foo() {} //go-cov:skip
			`,
			[]skippedBlock{{3, 3}},
		},
		{
			"skipped function with additional commet",
			`package somePackage

			func foo() {} //some-other-tool:blah //go-cov:skip
			`,
			[]skippedBlock{{3, 3}},
		},
		{
			"skipped function, comment not at start",
			`package somePackage

			func foo() {} //some-other-tool:blah //go-cov:skip
			`,
			[]skippedBlock{{3, 3}},
		},
		{
			"skipped function with extra comment",
			`package somePackage

			func foo() {} //go-cov:skip some more detail, explaining why we skipped
			`,
			[]skippedBlock{{3, 3}},
		},
		{
			"skipped if",
			`package somePackage

			func foo() {
				if false { //go-cov:skip
				}
			}
			`,
			[]skippedBlock{{4, 5}},
		},
		{
			"skipped switch statement",
			`package somePackage

			func foo(bar int) int {
				switch bar { //go-cov:skip
				case 1:
					return 1
				case 2:
				}
			}
			`,
			[]skippedBlock{{4, 8}},
		},
		{
			"skipped switch case",
			`package somePackage

			func foo(bar int) int {
				switch bar {
				case 1: //go-cov:skip
					return 1
				case 2: //go-cov:skip
				}
			}
			`,
			[]skippedBlock{{5, 6}, {7, 7}},
		},
		{
			"multiple skipped blocks",
			`package somePackage

			func foo() {
				if false { //go-cov:skip
				}

				unusedVar := 1

				for { //go-cov:skip
				}

				{ //go-cov:skip
				}
			}
			`,
			[]skippedBlock{
				{4, 5},
				{9, 10},
				{12, 13},
			},
		},
	}

	tempDir := t.TempDir()
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			fileName := filepath.Join(tempDir, tc.desc)
			require.NoError(t, os.WriteFile(fileName, []byte(tc.fileContent), 0o600))
			file, fset, err := parseFile(fileName)
			require.NoError(t, err)

			gotBlocks := findSkippedBlocks(file, fset)
			assert.Equal(t, tc.expectedBlocks, gotBlocks)
		})
	}
}
