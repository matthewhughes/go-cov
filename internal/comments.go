package internal

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"strings"
)

const _skipComment = "//go-cov:skip"

type skippedBlock struct {
	Start int
	End   int
}

func parseFile(path string) (*ast.File, *token.FileSet, error) {
	fset := token.NewFileSet()
	astFile, err := parser.ParseFile(
		fset,
		path,
		nil,
		parser.ParseComments|parser.SkipObjectResolution,
	)
	if err != nil {
		return nil, nil, fmt.Errorf("parsing file: %v", err)
	}

	return astFile, fset, nil
}

func findSkippedBlocks(file *ast.File, fset *token.FileSet) []skippedBlock {
	skipLines := make(map[int]struct{})
	for _, group := range file.Comments {
		for _, comment := range group.List {
			if strings.Contains(comment.Text, _skipComment) {
				skipLines[fset.Position(comment.Slash).Line] = struct{}{}
			}
		}
	}
	finder := skippedBlockFinder{fset, skipLines, []skippedBlock{}}
	ast.Walk(&finder, file)

	return finder.blocks
}

func isSkipped(file *ast.File, fset *token.FileSet) bool {
	for _, group := range file.Comments {
		for _, comment := range group.List {
			if comment.Text == _skipComment && fset.Position(comment.Slash).Column == 1 {
				return true
			}
		}
	}
	return false
}

type skippedBlockFinder struct {
	fset      *token.FileSet
	skipLines map[int]struct{}
	blocks    []skippedBlock
}

func (self *skippedBlockFinder) Visit(node ast.Node) ast.Visitor {
	switch node := node.(type) {
	case *ast.BlockStmt:
		if _, ok := self.skipLines[self.fset.Position(node.Lbrace).Line]; ok {
			self.blocks = append(
				self.blocks,
				skippedBlock{
					self.fset.Position(node.Lbrace).Line,
					self.fset.Position(node.Rbrace).Line,
				},
			)
		}
	case *ast.CaseClause:
		if _, ok := self.skipLines[self.fset.Position(node.Colon).Line]; ok {
			body := node.Body
			// case with no statements
			if len(body) == 0 {
				self.blocks = append(
					self.blocks,
					skippedBlock{
						self.fset.Position(node.Colon).Line,
						self.fset.Position(node.Colon).Line,
					},
				)
			} else {
				self.blocks = append(
					self.blocks,
					skippedBlock{
						self.fset.Position(node.Colon).Line,
						self.fset.Position(body[len(body)-1].Pos()).Line,
					},
				)
			}
		}
	}

	return self
}
