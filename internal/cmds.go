package internal

import (
	"fmt"
	"io"

	"github.com/urfave/cli/v2"
	"golang.org/x/tools/cover"
)

func AddSkipsCmd(out io.Writer) *cli.Command {
	return &cli.Command{
		Name:      "add-skips",
		Usage:     "Update a coverage profile with skip information",
		ArgsUsage: "<coverage-profile>",
		Action: func(c *cli.Context) error {
			return PrintProfilesWithSkips(c.Args().Slice(), out)
		},
	}
}

func ReportCmd(in io.Reader, out io.Writer) *cli.Command {
	return &cli.Command{
		Name:      "report",
		Usage:     "Report coverage from a cover profile. Reads profile from stdin if none provided",
		ArgsUsage: "[ coverage-profile ]",
		Flags: []cli.Flag{
			&cli.Float64Flag{
				Name:  "fail-under",
				Value: 0.0,
				Usage: "Fail if the total coverage is less than this value as a percent (e.g. 100, 85, 55.5))",
			},
		},
		Action: func(c *cli.Context) error {
			return reportCoverage(in, out, c.Float64("fail-under"), c.Args().Slice())
		},
	}
}

func reportCoverage(in io.Reader, out io.Writer, failUnder float64, args []string) error {
	if len(args) > 1 {
		return fmt.Errorf("takes at most one argument, but %d provided", len(args))
	}

	var profiles []*cover.Profile
	var err error
	switch len(args) {
	case 1:
		covFile := args[0]
		profiles, err = cover.ParseProfiles(covFile)
		if err != nil {
			return fmt.Errorf("parsing profiles from %s: %v", covFile, err)
		}
	case 0:
		profiles, err = cover.ParseProfilesFromReader(in)
		if err != nil {
			return fmt.Errorf("parsing profiles from input: %v", err)
		}
	}

	blockCount := 0
	uncoveredBlockCount := 0
	for _, profile := range profiles {
		for _, block := range profile.Blocks {
			if block.Count == 0 {
				fmt.Fprintf(
					out,
					"Uncovered block in %s at (line, col) (%d, %d)\n",
					profile.FileName,
					block.StartLine,
					block.StartCol,
				)
				uncoveredBlockCount++
			}
			blockCount++
		}
	}

	covPercent := (1.0 - float64(uncoveredBlockCount)/float64(blockCount)) * 100.0
	fmt.Fprintf(out, "coverage: %.02f%%\n", covPercent)
	if covPercent < failUnder {
		return fmt.Errorf("coverage (%.02f%%) below limit (%.02f%%)", covPercent, failUnder)
	}

	return nil
}
