package internal

import (
	"path/filepath"
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/tools/cover"
)

func Test_updateWithSkips(t *testing.T) {
	testCases := []struct {
		desc           string
		profileBlocks  []cover.ProfileBlock
		skips          []skippedBlock
		expectedCounts []int
	}{
		{
			"Two single blocks, with overlap, no covered",
			[]cover.ProfileBlock{
				{StartLine: 1, EndLine: 4},
			},
			[]skippedBlock{
				{Start: 1, End: 4},
			},
			[]int{1},
		},
		{
			"Two single blocks, with overlap, covered",
			[]cover.ProfileBlock{
				{StartLine: 1, EndLine: 4, Count: 10},
			},
			[]skippedBlock{
				{Start: 1, End: 4},
			},
			[]int{10},
		},
		{
			"Two single blocks, no overlap",
			[]cover.ProfileBlock{
				{StartLine: 1, EndLine: 4},
			},
			[]skippedBlock{
				{Start: 5, End: 10},
			},
			[]int{0},
		},
		{
			"Two blocks, entirely overlap with skip",
			[]cover.ProfileBlock{
				{StartLine: 1, EndLine: 4},
				{StartLine: 5, EndLine: 10},
			},
			[]skippedBlock{
				{Start: 1, End: 4},
				{Start: 5, End: 10},
			},
			[]int{1, 1},
		},
		{
			"Several blocks, some overlap",
			[]cover.ProfileBlock{
				{StartLine: 1, EndLine: 4},
				{StartLine: 10, EndLine: 13},
			},
			[]skippedBlock{
				{Start: 1, End: 3},
				{Start: 20, End: 25},
			},
			[]int{1, 0},
		},
		{
			"Several cover blocks, single AST block",
			[]cover.ProfileBlock{
				{StartLine: 9, EndLine: 12},
				{StartLine: 12, EndLine: 24},
				{StartLine: 24, EndLine: 40},
			},
			[]skippedBlock{
				{Start: 9, End: 40},
			},
			[]int{1, 1, 1},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			profile := cover.Profile{Blocks: tc.profileBlocks}

			updateWithSkips(&profile, tc.skips)
			gotCounts := make([]int, len(profile.Blocks))
			for i, block := range profile.Blocks {
				gotCounts[i] = block.Count
			}
			assert.Equal(t, tc.expectedCounts, gotCounts)
		})
	}
}

func Test_getPathForProfile(t *testing.T) {
	testCases := []struct {
		desc         string
		profile      cover.Profile
		packageMap   map[string]string
		expectedPath string
		expectedOk   bool
	}{
		{
			"local package returned directly",
			cover.Profile{FileName: "./path/to/local"},
			map[string]string{},
			"./path/to/local",
			true,
		},
		{
			"package with supported prefix returned",
			cover.Profile{FileName: "example.com/sample/package/file.go"},
			map[string]string{"example.com/sample/package": "/some/path"},
			"/some/path/file.go",
			true,
		},
		{
			"package without map entry not found",
			cover.Profile{FileName: "example.com/sample/package/file.go"},
			map[string]string{"example.com/other/package": "/some/path"},
			"",
			false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			gotPath := getPathForProfile(&tc.profile, tc.packageMap) // #nosec: G601

			assert.Equal(t, tc.expectedPath, gotPath)
		})
	}
}

func Test_getPackagePaths(t *testing.T) {
	packageName := "gitlab.com/matthewhughes/go-cov/internal"
	profile := cover.Profile{FileName: packageName + "/profiles.go"}
	_, file, _, ok := runtime.Caller(0)
	require.True(t, ok)
	expected := map[string]string{packageName: filepath.Dir(file)}

	got, err := getPackagePaths([]*cover.Profile{&profile})

	require.NoError(t, err)
	assert.Equal(t, expected, got)
}
