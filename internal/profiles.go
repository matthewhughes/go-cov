package internal

import (
	"errors"
	"fmt"
	"path"
	"path/filepath"
	"strings"

	"golang.org/x/tools/cover"
	"golang.org/x/tools/go/packages"
)

func hasLocalPackage(profile *cover.Profile) bool {
	return strings.HasPrefix(profile.FileName, ".") || filepath.IsAbs(profile.FileName)
}

func getPathForProfile(profile *cover.Profile, packagePathMap map[string]string) string {
	if hasLocalPackage(profile) {
		return profile.FileName
	}

	for importPath, dir := range packagePathMap {
		if strings.HasPrefix(profile.FileName, importPath) {
			return dir + profile.FileName[len(importPath):]
		}
	}
	return ""
}

func getPackagePaths(profiles []*cover.Profile) (map[string]string, error) {
	packagePaths := make(map[string]string)
	var packageNames []string

	for _, profile := range profiles {
		if hasLocalPackage(profile) {
			continue
		}
		packageName := path.Dir(profile.FileName)
		if _, ok := packagePaths[packageName]; !ok {
			packagePaths[packageName] = ""
			packageNames = append(packageNames, packageName)
		}
	}

	packages, err := packages.Load(
		&packages.Config{Mode: packages.NeedName | packages.NeedFiles},
		packageNames...,
	)
	if err != nil { //go-cov:skip
		return nil, fmt.Errorf("loading packages: %v", err)
	}

	for _, pkg := range packages {
		if len(pkg.Errors) > 0 {
			errString := "failed to list packages: "
			for _, err := range pkg.Errors {
				errString += err.Error()
			}
			return nil, errors.New(errString)
		}
		if len(pkg.GoFiles) > 0 {
			packagePaths[pkg.PkgPath] = filepath.Dir(pkg.GoFiles[0])
		}
	}

	return packagePaths, nil
}

func updateWithSkips(profile *cover.Profile, skips []skippedBlock) {
	if len(skips) == 0 {
		return
	}

	blocks := profile.Blocks
	skipIndex := 0
	blockIndex := 0

	for blockIndex < len(blocks) {
		block := &blocks[blockIndex]
		if block.StartLine >= skips[skipIndex].End && skipIndex < len(skips)-1 {
			skipIndex++
		}
		skip := skips[skipIndex]
		if block.StartLine >= skip.Start && block.StartLine <= skip.End {
			if block.Count == 0 {
				block.Count = 1
			}
		}
		blockIndex++
	}
}

func setAllBlocksSkipped(profile *cover.Profile) {
	blocks := profile.Blocks
	for i := range blocks {
		if blocks[i].Count == 0 {
			blocks[i].Count = 1
		}
	}
}

func stringifyProfile(profile *cover.Profile) string {
	res := ""
	for i := range profile.Blocks {
		res += fmt.Sprintln(stringifyBlock(profile.FileName, &profile.Blocks[i]))
	}
	return res
}

func stringifyBlock(fileName string, block *cover.ProfileBlock) string {
	return fmt.Sprintf(
		"%s:%d.%d,%d.%d %d %d",
		fileName,
		block.StartLine,
		block.StartCol,
		block.EndLine,
		block.EndCol,
		block.NumStmt,
		block.Count,
	)
}
