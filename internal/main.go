package internal

import (
	"fmt"
	"io"

	"dmitri.shuralyov.com/go/generated"
	"golang.org/x/tools/cover"
)

func PrintProfilesWithSkips(args []string, out io.Writer) error {
	if len(args) != 1 {
		return fmt.Errorf("takes exactly one argument, but %d provided", len(args))
	}
	covFile := args[0]

	profiles, err := cover.ParseProfiles(covFile)
	if err != nil {
		return fmt.Errorf("parsing profiles from %s: %v", covFile, err)
	}
	if len(profiles) == 0 {
		return nil
	}

	packagePaths, err := getPackagePaths(profiles)
	if err != nil {
		return fmt.Errorf("listing paths for packages: %s", err)
	}

	fmt.Fprintf(out, "mode: %s\n", profiles[0].Mode)
	for _, profile := range profiles {
		if err := printProfileWithSkip(out, profile, packagePaths); err != nil {
			return err
		}
	}

	return nil
}

func printProfileWithSkip(
	out io.Writer,
	profile *cover.Profile,
	packagePaths map[string]string,
) error {
	path := getPathForProfile(profile, packagePaths)
	if path != "" {
		file, fset, err := parseFile(path)
		if err != nil {
			return fmt.Errorf("parsing %s: %s", path, err)
		}
		isGenerated, err := generated.ParseFile(path)
		if err != nil { //go-cov:skip // we can already parse file for ast, so don't expect this error
			return fmt.Errorf("checking if file is generated: %w", err)
		}
		if isSkipped(file, fset) || isGenerated {
			setAllBlocksSkipped(profile)
		} else {
			blocks := findSkippedBlocks(file, fset)
			updateWithSkips(profile, blocks)
		}
		fmt.Fprint(out, stringifyProfile(profile))
	}

	return nil
}
