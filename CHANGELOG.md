# Changelog

## 0.5.0 - 2025-02-28

### Added

  - Allow skipping entire files
  - Add automatic skipping of generated files

### Removed

  - Remove support for Go 1.21
  - Remove support for Go 1.22

## 0.4.0 - 2024-05-09

### Changed

  - Make handling of `SIGINT` more graceful

## 0.3.0 - 2024-03-30

### Changed

  - **Breaking**: base functionality moved to subcommand: run `go-cov add-skips`
    instead of bare `go-cov`

### Added

  - Add subcommand to report on coverage: `go-cov report`

## 0.2.0 - 2023-06-30

### Changed

  - Allow skip comment to be placed anywhere in a comment

### Fixed

  - Fixed block incorrectly marked as not skipped

## 0.1.0 - 2023-04-15

Initial release
