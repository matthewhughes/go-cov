# `go-cov`

[![coverage
report](https://gitlab.com/matthewhughes/go-cov/badges/main/coverage.svg)](https://gitlab.com/matthewhughes/go-cov/-/commits/main)

Generate coverage profiles with the ability to explicitly exclude lines,
inspired by [the exclusion functionality of
`coverage.py`](https://coverage.readthedocs.io/en/7.2.3/excluding.html).

Having a coverage report that excludes lines you don't intend to cover can be
helpful in maintaining test coverage, since it makes adding code not covered by
tests a conscious decision. Knowing what you do/do not intend to cover with
tests is also useful for example when looking back over old code and trying to
determine why something may not already be covered by tests.

It also allows you to aim to have 100% coverage without having to cover every
single line. This is then much easier to maintain without any extra tooling:
just make sure coverage is always 100%.

## Installation

`go-cov` can be installed using `go` via:

    $ go install gitlab.com/matthewhughes/go-cov/cmd/go-cov

## Excluding Lines

Blocks can be excluded by adding a `//go-cov:skip` comment at the start of the
block:

``` go
func unTestable() { //go-cov:skip
	// do stuff...
}

func someFunc(debug bool) {
	// do stuf...

	// the comment doesn't need to start with the skip
	if debug { //some-other-tool:blah //go-cov:skip
		log.Println("This is some useful debug information")
	}

	if someHardToTestCondition { //go-cov:skip // explaination for not testing this
	}
}
```

### Skipping entire files

Note: generated files (as determined by
[`dmitri.shuralyov.com/go/generated`](https://pkg.go.dev/dmitri.shuralyov.com/go/generated))
will automatically be skipped.

If you want to exclude all blocks in a file you can add a top-level
`//go-cov:skip` comment:

``` go
package lib

//go-cov:skip

import (
	"fmt"
)

func someFunc() {
	fmt.Println("Hello")
}

func someOtherFunc() {
	// ... do things
}
```

## Usage

### Update coverage profile with skips

Takes a coverage profile (i.e. one generated with `go test -coverprofile`) and
adds coverage for any blocks starting with a `go-cov:skip` comment. The result
is written to stdout. Sample usage:

``` console
# generate the coverage profile
$ go test -coverprofile=coverage.out ./...
# write the updated profile to `go-cov.out`
$ go-cov add-skips coverage.out > go-cov.out
# investigate the result
$ go tool cover -func=go-cov.out
```

### Report coverage

The `report` command reports coverage in a human readable manner and can
optionally fail if coverage is below a required level. Sample usage:

``` console
$ go-cov report coverage.out
# fail if less than 80% of lines are covered
$ go-cover report --fail-under 80 coverage.out
# the coverage profiles can be read from stdin
$ go-cover add-skips coverage.out | go-cover report --fail-under 90
```
